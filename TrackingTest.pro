QT += core
QT += gui

TARGET = TrackingTest
CONFIG += console
CONFIG -= app_bundle

QMAKE_CXXFLAGS += -std=c++14

TEMPLATE = app

SOURCES += main.cpp

INCLUDEPATH +='/usr/include/x86_64-linux-gnu/qt5/'

INCLUDEPATH += `pkg-config --cflags opencv`
LIBS += `pkg-config --libs opencv`

